import React, { useState } from 'react';
import MyButton from './components/button/MyButton';
import MyInput from './components/input/MyInput';
import PostList from './components/PostList';
import './styles/App.css';





function App() {
  const [posts, setPosts] = useState([
    { id: 1, title: 'JavaScript1', body: 'Discription' },
    { id: 2, title: 'JavaScript2', body: 'Discription' },
    { id: 3, title: 'JavaScript3', body: 'Discription' }
  ])


  const addNewPost = () => {

  }

  return (
    <div className="App">
      <form>
        <MyInput type="text" placeholder='Название текста' />
        <MyInput type="text" placeholder='Описание текста' />
        <MyButton onClick={addNewPost}>Создать пост</MyButton>
      </form>
      <PostList posts={posts} title="Список постов" />
    </div>
  );
}

export default App;
